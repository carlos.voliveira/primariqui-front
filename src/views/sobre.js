import React from 'react'

const Sobre = () => {

  const [salario, setSalario] = React.useState(1000);
  const [info] = React.useState({
    falta: 10,
    hExtra: 100,
    imposto: salario * 0.12
  })

  const add = () => {
    console.log('add 100 salário')
    setSalario(salario + info.hExtra)
  }

  const desc = () => {
    console.log('desc 10 salário')
    setSalario(salario - info.falta)
  }

  const imp = () => {
    console.log('desc 12% salário')
    setSalario(salario - info.imposto)
  }

  return (
    <div>
      <strong>Salário:</strong> { salario }
      <hr />
      <button onClick = { add }>Acrescentar uma comissão +100</button>
      <br />
      <br />
      <button onClick = { desc }>desconto por falta -10</button>
      <br />
      <br />
      <button onClick = { imp }>impostos 12%</button>

    </div>
  )
}

export default Sobre