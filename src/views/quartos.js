import React, { useState, useEffect, useCallback } from 'react';
import http from '../config/http'


const Quartos = () => {
  //controle de estado LOCAL
  //const [ valorAtual, funcaoQueMudaOValor ] = React.useState();
  const [nome] = useState("Primariqui Hostel");
  const [quartos, setQuartos] = useState([]);

  // const mudarNome = () => {
  //   console.log('testando a função');
  //   setNome('primari-teste-qui')
  // }

  /*
    1- chamar a api
    2- guardar o resultado da api em um estado
  */
  const getQuartos = useCallback(
    async () => {
      try {
        const res = await http.get('/v1/quarto');
        setQuartos(res.data);
        
      } catch (err) {
          console.log('###', err)
      }
      
    },
    [],
  ) 
  
  
  useEffect(() => {
    
    getQuartos();

    // setQuarto(resultadoAxios)
  }, [getQuartos])

  return (
    <div className='quartos'>
      { nome }
      <ul>
        {quartos.map((item, i) => (
          <li key={i}>{item.name}</li>
        ))}
      </ul>
      <hr />

      {/* <button onClick={() => setNome('primari-teste-qui')}>
        clique aqui para mudar o nome
      </button> */}
    </div>
  );
}

export default Quartos