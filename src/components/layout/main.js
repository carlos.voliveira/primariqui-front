const Main = (props) => {
  return (
    <div>
      <main>
        { props.content }
      </main>
    </div>
  )
}

export default Main
