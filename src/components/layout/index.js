import Header from './header'
import Main from './main'
import Footer from './footer'

const Layout = ({ children, page }) => {
  // console.log(name);
  document.title = page;
  return (
    <div>
      
      <Header titulo = { page } />

      <Main content = { children } />

      <Footer titulo = { page } />
      
    </div>
  )
}

export default Layout;