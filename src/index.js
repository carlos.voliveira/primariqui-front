import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/style.css'

// componentes básicos
import Layout from './components/layout';

// views
import Quartos from './views/quartos';
// import Sobre from './views/sobre';

ReactDOM.render(
  <React.Fragment>
    <Layout page="Hostel Primariqui">
      <Quartos />
      {/* <Sobre /> */}
    </Layout>
  </React.Fragment>,
  document.getElementById('root')
);