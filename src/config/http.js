import axios from 'axios'; // importação da dependência

// definindo a url da api
const urlApi = "http://localhost:3001"
// const urlApi = "http://localhost:3001/v1/quarto"


// criando um cliente http através do axios
const http = axios.create({
  baseURL: urlApi
});

// definindo o header padrão da aplicação
http.defaults.headers['content-type'] = 'aplication/json';

export default http;